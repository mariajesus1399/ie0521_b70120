import gzip
import sys
import re
import time
from collections import deque
import os

#Se reciben los parametros por consola
s = int(sys.argv[2])
bp = int(sys.argv[4])
gh = int(sys.argv[6])
ph = int(sys.argv[8])
o = int(sys.argv[10])
BHT = []
global_register = deque()
last_prediction = 0
PHT = []
MTP = []
correct_taken = 0
correct_not_taken = 0
incorrect_taken = 0
incorrect_not_taken = 0

#Funcion para procesar los datos sin descomprimir
def procesador_branch_traces():
    gzip_traces = sys.stdin.read()
    traces = gzip_traces.split("\n")
    return traces

#Funcion para construir e inicializar los estados de la Branch History Table en strongly not taken, sus entradas seran de 2^s
#Recibe el parametro s, tamano de la BHT
def construc_BHT(s):
    while len(BHT) < pow(2, s):
        BHT.append('00')

  

#Funcion para construir la tabla del metapredictor, se inicializa en strongly prefer private
def construc_MTP(s):
    while len(MTP) < pow(2, s):
       MTP.append('00')
    return MTP


#Funcion para construir e inicializar los registros de la Pattern History Table en 0, sus entradas seran de 2^s
#Recibe el parametro s, tamano de la BHT
def construc_PHT(s):
    while len(PHT) < pow(2, s):
       PHT.append(deque())
    return PHT


#Funcion para determinar los ultimos s bits en binario
#Recibe el program counter y el parametro s, tamano de la BHT
def determinate_index(PC,s):
    PC_binary = bin(int(PC))
    PC_binary_re = re.search('[0-9]*$', PC_binary)
    PC_b =PC_binary_re.group(0)
    index_binary = PC_b[-s:]
    index_decimal = int(index_binary,2)
    return index_decimal, PC_b

#Funcion para determinar la prediccion segun la condicion del counter en el index
#Recibe la branch history table y el index de la tabla
def two_bit_counter(Table, index):
    condition_of_specific_counter = Table[index]
    if condition_of_specific_counter == '00':   
        return 'N'
    elif condition_of_specific_counter == '01':
        return 'N'
    elif condition_of_specific_counter == '10':       
        return 'T'
    elif condition_of_specific_counter == '11':       
        return 'T'


#Funcion para cambiar el estado del counter luego del ultimo salto
def change_status_counter(Outcome, Table, index):
    condition_of_specific_counter = Table[index]
    if Outcome == 'T':
        #construc_global_register(gh, 1)
        if condition_of_specific_counter =='00':
            Table[index] = '01'
        elif condition_of_specific_counter =='01':
            Table[index] = '10'
        elif condition_of_specific_counter =='10':
            Table[index] = '11'
        elif condition_of_specific_counter =='11':
            Table[index] = '11'
    else :
        #construc_global_register(gh, 0)
        if condition_of_specific_counter =='00':
            Table[index] = '00'
        elif condition_of_specific_counter =='01':
            Table[index] = '00'
        elif condition_of_specific_counter =='10':
            Table[index] = '01'
        elif condition_of_specific_counter =='11':
            Table[index] = '10'

#Funcion para determinar si la prediccion es correcta o no 
def result(prediction, Outcome):   
    if prediction == Outcome :
        return 'Correct'
    else:
        return 'Incorrect'

def register_branches(is_correct, prediction):
    global correct_taken
    global correct_not_taken 
    global incorrect_taken
    global incorrect_not_taken
    if (is_correct == "Correct" and prediction == "T"):
        correct_taken = correct_taken + 1      
    elif (is_correct == "Correct" and prediction == "N"):
        correct_not_taken = correct_not_taken + 1  
    elif (is_correct == "Incorrect" and prediction == "T"):
        incorrect_taken = incorrect_taken + 1 
    elif (is_correct == "Incorrect" and prediction == "N"):
        incorrect_not_taken = incorrect_not_taken + 1

#Funcion principal para el predictor bimodal, se encarga de llamar a las otras funciones 
def bimodal_predictor(traces, x):
            line = traces[x]
            PC_and_Outcome = line.split(" ")
            PC = PC_and_Outcome[0]
            Outcome = PC_and_Outcome[1]
            index = determinate_index(PC,s)
            index_decimal = index[0]
            prediction = two_bit_counter(BHT, index_decimal)
            change_status_counter(Outcome, BHT, index_decimal)
            is_correct = result(prediction, Outcome)
            register_branches(is_correct, prediction)
            return PC, Outcome, prediction, is_correct

        

#Funcion para construir el global register de tamano gh, se saca el primer elemento ingresado cuando ya esta lleno 
def construc_global_register(gh, last_result):
    if len(global_register) < gh:
        global_register.append(last_result)    
    else:
        #The first element insert is delate
        global_register.popleft() 
        global_register.append(last_result)   

#Funcion para operacion xor entre 2 datos y tomar sus lsb 
def xor(data1, data2, index_decimal):
    if len(data1) == 0: 
        return index_decimal
    else: 
        xor = int(data1,2) ^ int(data2,2)
        xor_binario = '{0:b}'.format(xor)
        xor_lsb = xor_binario[-s:]
        xor_lsb_decimal = int(xor_lsb,2)    
        return xor_lsb_decimal;

#Funcion principal para el predictor global, se encarga de llamar a otras funciones 
def global_history_predictor(traces, x)  :
            line = traces[x]
            PC_and_Outcome = line.split(" ")
            PC = PC_and_Outcome[0]
            Outcome = PC_and_Outcome[1]
            index = determinate_index(PC,s)
            index_decimal = index[0]
            PC_binary = index [1]
            global_register_str = ""
            for register in global_register: 
                global_register_str = global_register_str + str(register)
            index_xor = xor(global_register_str, PC_binary, index_decimal)     
            prediction = two_bit_counter(BHT, index_xor)
            change_status_counter(Outcome, BHT, index_xor)
            is_correct = result(prediction, Outcome)
            register_branches(is_correct, prediction)
            if (Outcome == 'T'):
                construc_global_register(gh, 1)
            else: 
                construc_global_register(gh, 0)
            return PC, Outcome, prediction, is_correct



#Funcion para construir el private register de tamano gh, se saca el primer elemento ingresado cuando ya esta lleno 
def construc_private_register(ph, last_result, index):
    if len(PHT[index]) < ph:
        PHT[index].append(last_result)    
    else:
        PHT[index].popleft() 
        PHT[index].append(last_result)   

#Funcion para el predictor global, se encarga de llamar a otras funciones

def private_history_predictor(traces, x, PHT):
            line = traces[x]
            PC_and_Outcome = line.split(" ")
            PC = PC_and_Outcome[0]
            Outcome = PC_and_Outcome[1]
            index = determinate_index(PC,s)
            index_decimal = index[0]
            PC_binary = index [1]
            private_register = PHT[index_decimal]
            private_register_str = ""
            for register in private_register: 
                private_register_str = private_register_str + str(register)
            index_xor = xor(private_register_str, PC_binary, index_decimal)   
            prediction = two_bit_counter(BHT, index_xor)
            change_status_counter(Outcome, BHT, index_xor)
            is_correct = result(prediction, Outcome)
            register_branches(is_correct, prediction)
            condition_of_specific_counter = BHT[index_xor]
            if (Outcome == 'T'):
                construc_private_register(ph, 1, index_decimal)       
            else: 
                construc_private_register(ph, 0, index_decimal)  
            return PC, Outcome, prediction, is_correct 


 
#Funcion para convertir deque a string
def convert_register_string(deque_register):
    register_str = ""
    for register in deque_register: 
        register_str = register_str + str(register)
    return register_str

#Funcion del predictor global para el de torneo
def global_predictor_generic(traces, x):
            line = traces[x]
            PC_and_Outcome = line.split(" ")
            PC = PC_and_Outcome[0]
            Outcome = PC_and_Outcome[1]
            index = determinate_index(PC,s)
            index_decimal = index[0]
            PC_binary = index [1]
            global_register_str = ""
            for register in global_register: 
                global_register_str = global_register_str + str(register)
            index_xor = xor(global_register_str, PC_binary, index_decimal)     
            prediction = two_bit_counter(BHT, index_xor)
            #change_status_counter(Outcome, BHT, index_xor)
            #is_correct = result(prediction, Outcome)
            if (Outcome == 'T'):
                construc_global_register(gh, 1)
            else: 
                construc_global_register(gh, 0)
            return prediction, index_xor

#Funcion del predictor privado para el de torneo
def private_predictor_generic(traces, x, PHT):
            line = traces[x]
            PC_and_Outcome = line.split(" ")
            PC = PC_and_Outcome[0]
            Outcome = PC_and_Outcome[1]
            index = determinate_index(PC,s)
            index_decimal = index[0]
            PC_binary = index [1]
            private_register = PHT[index_decimal]
            private_register_str = ""
            for register in private_register: 
                private_register_str = private_register_str + str(register)
            index_xor = xor(private_register_str, PC_binary, index_decimal)   
            prediction = two_bit_counter(BHT, index_xor)
            if (Outcome == 'T'):
                construc_private_register(ph, 1, index_decimal)       
            else: 
                construc_private_register(ph, 0, index_decimal)  
            return prediction, index_xor


#Funcion para determinar la prediccion segun la condicion del counter en el index
#Recibe la branch history table y el index de la tabla
def bc_MTP(Table, index):
    condition_of_specific_counter = Table[index]
    if condition_of_specific_counter == '00':   
        return 'P'
    elif condition_of_specific_counter == '01':
        return 'P'
    elif condition_of_specific_counter == '10':       
        return 'G'
    elif condition_of_specific_counter == '11':       
        return 'G'

#Funcion para cambiar el estado del metapredictor para elegir cual predictor de saltos escoger
def change_status_MTP(Table, metapredictor, index, is_correct_global, is_correct_private):
    if (metapredictor == "P"):
        if (is_correct_global == "Correct" and is_correct_private == "Correct"):
                Table[index] = '00'
        elif (is_correct_private == "Correct" and is_correct_global == "Incorrect"):
                Table[index] = '01'
        elif (is_correct_global == "Correct" and is_correct_private == "Incorrect"):
                Table[index] = '10'
        elif (is_correct_global == "Incorrect" and is_correct_private == "Incorrect"):
                Table[index] = '11'

    else:
        if (is_correct_global == "Correct" and is_correct_private == "Correct"):
                Table[index] = '11'
        elif (is_correct_global == "Correct" and is_correct_private == "Incorrect"):
                Table[index] = '10'
        elif (is_correct_global == "Incorrect" and is_correct_private == "Correct"):
                Table[index] = '01'
        elif (is_correct_global == "Incorrect" and is_correct_private == "Incorrect"):
                Table[index] = '00'


        

#Funcion principal para el predictor de torneo, se encarga de llamar a los otras funciones

def tournament_predictor(traces, x, MTP, PHT):
    line = traces[x]
    PC_and_Outcome = line.split(" ")
    PC = PC_and_Outcome[0]
    Outcome = PC_and_Outcome[1]
    index = determinate_index(PC,s)
    index_decimal = index[0]
    globalhistory = global_predictor_generic(traces, x)
    privatehistory = private_predictor_generic(traces, x, PHT)
    prediction_global = globalhistory[0]
    prediction_private = privatehistory[0]
    is_correct_global = result(prediction_global, Outcome)
    is_correct_private= result(prediction_private, Outcome)
    metapredictor = bc_MTP(MTP, index_decimal)
    change_status_MTP(MTP, metapredictor, index_decimal, is_correct_global, is_correct_private)
    if (metapredictor == 'P'):
        prediction = prediction_private
        index_xor = privatehistory[1]
        change_status_counter(Outcome, BHT, index_xor)
        is_correct = result(prediction, Outcome)
        register_branches(is_correct, prediction)
        return PC, metapredictor, Outcome, prediction, is_correct   
    else: 
        prediction = prediction_global
        index_xor = globalhistory[1]
        change_status_counter(Outcome, BHT, index_xor)
        is_correct = result(prediction, Outcome)
        register_branches(is_correct, prediction)
        return PC, metapredictor, Outcome, prediction, is_correct
   
#Funcion para llamar al predictores escogido e imprimir los primeros 5000 mil resultados en un archivo de salida
def report_results(traces):
    if (bp == 0):
        file = open("bimodal_predictor.txt", "w")
        file.write("PC"+ "     " + "Outcome" + "     " + "Prediction" + "     "+ "Correct/Incorrect" + os.linesep)
        for x in range (5000):
            bimodal = bimodal_predictor(traces, x)
            file.write(bimodal[0]+ "     " +bimodal[1] + "     " + bimodal[2] + "     "+ bimodal[3] + os.linesep)
        file.close()
    elif (bp == 1):
        file = open("global_history_predictor.txt", "w")
        file.write("PC"+ "     " + "     Outcome" + "     " + "Prediction" + "     "+ "Correct/Incorrect" + os.linesep)  
        for x in range (5000):
            globalhistory = global_history_predictor(traces, x)  
            file.write(globalhistory[0]+ "     " +globalhistory[1] + "         " + globalhistory[2] + "          "+ globalhistory[3] + os.linesep)  
        file.close()
    elif (bp == 2):
        PHT = construc_PHT(s)  
        file = open("private_history_predictor.txt", "w")
        file.write("PC"+ "     " + "     Outcome" + "     " + "Prediction" + "     "+ "Correct/Incorrect" + os.linesep)  
        for x in range (5000):
            privatehistory = private_history_predictor(traces, x, PHT)   
            file.write(privatehistory[0]+ "     " +privatehistory[1] + "     " + privatehistory[2] + "     "+ privatehistory[3] + os.linesep)  
        file.close()
    elif (bp == 3): 
        PHT = construc_PHT(s)  
        MTP = construc_MTP(s)
        file = open("tournament_predictor.txt", "w")
        file.write("PC"+ "     " + "     Outcome" + "     " + "Prediction" + "     "+ "Correct/Incorrect" + os.linesep)  
        for x in range (5000):
            tournament = tournament_predictor(traces, x, MTP, PHT)  
            file.write(tournament[0]+ "     " +tournament[1] + "     " + tournament[2] + "     "+ tournament[3] +  "      " + tournament[4] + os.linesep)  
        file.close()
#Funcion para simular los predictores sin escribir en ningun archivo
def simulation(traces):
    if (bp == 0):
        for x in range (5000):
            bimodal = bimodal_predictor(traces, x)
    elif (bp == 1):
        for x in range (5000):
            globalhistory = global_history_predictor(traces, x)  
    elif (bp == 2):
        PHT = construc_PHT(s)  
        for x in range (5000):
            privatehistory = private_history_predictor(traces, x, PHT)   
    elif (bp == 3): 
        PHT = construc_PHT(s)  
        MTP = construc_MTP(s)
        for x in range (5000):
            tournament = tournament_predictor(traces, x, MTP, PHT)  
#Funcion para imprimir parametros por consola
def print_console():
    print ("Prediction parameters")
    if (bp == 0):
        print ("  Branch prediction type: Bimodal")
    elif (bp == 1):
        print ("  Branch prediction type: Global history")
    elif (bp == 2):
        print ("  Branch prediction type: Private history")
    elif (bp == 3):
        print ("  Branch prediction type:Tournament")
    print ("BHT size:                                         " + str(s))
    print ("History register size :                           " + str(gh))
    print ("Private history register size:                    " + str(ph))
    print ("Simulation results")
    print ("Number of branch:                                 5000" )
    print ("Number of correct prediction of taken branches:   " + str(correct_taken))
    print ("Number of incorrect prediction of taken branches: " + str(incorrect_taken))
    print ("Correct prediction of not taken branches:         " + str(correct_not_taken))
    print ("Incorrect prediction of not taken branches:       " + str(incorrect_not_taken))
    Percentage_correct = (100*(correct_taken + correct_not_taken))/5000
    print ("Percentage of correct predictions:                " + str(Percentage_correct)) 

#Funcion principal del programa
def main():
    traces = procesador_branch_traces()
    construc_BHT(s)
    if ( o == 1):
        report_results(traces)
        print_console()
    else: 
        simulation(traces)
        print_console()
main()
